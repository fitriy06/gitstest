<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Test extends CI_Controller {

		 public function __construct()
        {
                parent::__construct();
                $this->load->model('test_model');
		}
		public function index(){
			$this->coba_arr();
		}
		public function coba_arr(){
			$arr = $this->input->post('arr');
			$split = str_split($arr);
			$n = sizeof($split);
			$arr_ = array(1,0,0,1,0,1,1);
			$o = sizeof($arr_);
			$data['first'] = $this->test_model->getMaxLength($split, $n);
			$data['scnd'] = $this->test_model->getMaxLength($arr_, $o);
			$this->load->view('no1',$data);

		}
		public function no2(){
			if ($this->input->post("str")) {
				$data['reverse'] = $this->test_model->Reverse($this->input->post("str"));
			}else{
				$data['reverse'] = "";
			}
			$this->load->view('no2',$data);

		}
		public function no3(){
			$data['balance'] = $this->test_model->balaceBrackets($this->input->post("blc"));
			$this->load->view('no3',$data);

		}
		
}
?>