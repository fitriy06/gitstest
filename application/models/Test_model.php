<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Test_model extends CI_Model {

		public function getMaxLength($arr, $n)
		{
		    // intitialize count
		    $count = 0; 
		      
		    // initialize max
		    $result = 0; 
		  
		    for ($i = 0; $i < $n; $i++)
		    {
		        // Reset count when 0 is found
		        if ($arr[$i] != 1)
		            $count = 0;
		  
		        // If 1 is found, increment 
		        // count and update result
		        // if count becomes more.
		        else
		        {
		            // increase count
		            $count++;
		            $result = max($result, $count);
		        }
		    }
		  
		    return $result;
		}
		function Reverse($str){
      
		    // strlen() used to calculate the 
		    // length of the string
		    $len = strlen($str);
		  
		    // Base case for recursion
		    if($len == 1){
		        return $str;
		    }
		    else{
		        $len--;
		          
		        // extract first character and concatenate
		        // at end of string returned from recursive
		        // call on remaining string
		        return $this->Reverse(substr($str,1, $len)) 
		                        . substr($str, 0, 1);
		    }
		}
		function balaceBrackets($string) {
		    $len = strlen($string);
		    $stack = array();
		    for ($i = 0; $i < $len; $i++) {
		        switch ($string[$i]) {
		            case '(': array_push($stack, 0); break;
		            case ')': 
		                if (array_pop($stack) !== 0)
		                    return false;
		            break;
		            case '[': array_push($stack, 1); break;
		            case ']': 
		                if (array_pop($stack) !== 1)
		                    return false;
		            break;
		            case '{': array_push($stack, 2); break;
		            case '}': 
		                if (array_pop($stack) !== 2)
		                    return false;
		            break;
		            default: break;
		        }
		    }
		    return (empty($stack));
		}
		
}
?>